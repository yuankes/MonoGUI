// BWImgMgt.cpp: implementation of the BWImgMgt class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BWImgMgt::BWImgMgt()
{
	m_nCount = 0;
	m_nReserveSize = 0;
	m_pcaImages = NULL;
}

BWImgMgt::~BWImgMgt()
{
	Clear();
}

void BWImgMgt::Clear()
{
	delete [] m_pcaImages;
	m_nCount = 0;
	m_nReserveSize = 0;
	m_pcaImages = NULL;
}

BOOL BWImgMgt::Reserve (int nSize)
{
	if (nSize != m_nReserveSize) {

		BW_IMAGE_ITEM* temp_images = new BW_IMAGE_ITEM[nSize];
		if (temp_images == NULL) {
			return FALSE;
		}

		m_nReserveSize = nSize;

		int copy_size = min(m_nReserveSize, m_nCount);
		if (copy_size > 0) {
			memcpy (temp_images, m_pcaImages,
				sizeof(BW_IMAGE_ITEM) * copy_size);
		}
		if (m_pcaImages != NULL) {
			delete [] m_pcaImages;
		}
		m_pcaImages = temp_images;
		m_nCount = copy_size;
	}
	return TRUE;
}

BOOL BWImgMgt::Add (int nImgID, BW_IMAGE* pImg)
{
	if (m_nCount < m_nReserveSize) {

		m_pcaImages[m_nCount].id = nImgID;
		m_pcaImages[m_nCount].pImage = pImg;
		m_nCount ++;
	}
	else {

		if (! Reserve (m_nReserveSize + 1)) {
			return FALSE;
		}
		m_pcaImages[m_nCount].id = nImgID;
		m_pcaImages[m_nCount].pImage = pImg;
		m_nCount ++;
	}

	return TRUE;
}

BW_IMAGE* BWImgMgt::GetImage (int nImgID)
{
	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (m_pcaImages[i].id == nImgID)
			return m_pcaImages[i].pImage;
	}

	return NULL;
}

/* END */