// OSystemBar.cpp
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
OSystemBar::OSystemBar()
{
	m_nStatus  = 0;
	m_nBattery = 0;
	m_bCaps    = FALSE;
}

OSystemBar::~OSystemBar()
{
}

// 显示系统状态条；
void OSystemBar::Show (LCD* pLCD)
{
	if (m_nStatus == SYSBAR_STATE_HIDE)
		return;

	int crBk = 1;
	int crFr = 0;

	// 绘制底色和外框
	int x = SCREEN_W - SYSTEM_BAR_W;
	int y = 0;
	int w = SYSTEM_BAR_W;
	int h = SYSTEM_BAR_H;
	pLCD->FillRect(x, y, w, h, crBk);
	pLCD->HLine   (x, y+h-1, w, crFr);
	pLCD->VLine   (x, y, h, crFr);

	// 绘制电源/电池图标
	if (m_nStatus == SYSBAR_STATE_BAT_CHARGE)
	{
		pLCD->DrawImage(x+4, 1, 42, 13,
						g_4color_Power, 0, 0, LCD_MODE_NORMAL);
	}
	else
	{
		pLCD->DrawImage(x+4, 1, 42, 13,
						g_4color_Battery, 0, 0, LCD_MODE_NORMAL);
		// 绘制电量
		int nLen = (m_nBattery + 1) * 37 / 101;
		pLCD->FillRect(x+6, 3, nLen, 9, crFr);
	}

	// 绘制大写/小写图标
	if (m_bCaps) {
		pLCD->DrawImage(x+48, 1, 12, 13,
						g_4color_Captial, 0, 0, LCD_MODE_NORMAL);
	}
	else {
		pLCD->DrawImage(x+48, 1, 12, 13,
						g_4color_Lowercase, 0, 0, LCD_MODE_NORMAL);
	}
}

#if defined (MOUSE_SUPPORT)
// 鼠标点击切换大小写状态处理
BOOL OSystemBar::PtProc (int x, int y)
{
	if (m_nStatus == 0)
		return FALSE;

	int left   = SCREEN_W - SYSTEM_BAR_W + 48;
	int top    = 1;
	int right  = left + 12;
	int bottom = top  + 13;
	if (PtInRect (x, y, left, top, right, bottom))
	{
		if (m_bCaps)
			m_bCaps = FALSE;
		else
			m_bCaps = TRUE;

		return TRUE;
	}

	return FALSE;
}
#endif // defined(MOUSE_SUPPORT)

// 设置状态：0:不显示；1:显示充电；2:显示电池；
BOOL OSystemBar::SetStatus (int nStatus)
{
	m_nStatus = nStatus;
	return TRUE;
}

// 设置电池电量值，0 ~ 100；
BOOL OSystemBar::SetBattery (int nValue)
{
	if ((nValue<0) || (nValue >100))
		return FALSE;

	m_nBattery = nValue;
	return TRUE;
}

// 得到当前电池电量值；
int OSystemBar::GetBattery()
{
	return m_nBattery;
}

// 设置大小写状态指示；
BOOL OSystemBar::SetCaps (BOOL bValue)
{
	m_bCaps = bValue;
	return TRUE;
}

// 得到当前的大小写状态；
BOOL OSystemBar::GetCaps()
{
	return m_bCaps;
}

/* END */
