// OProgressBar.h: interface for the OProgressBar class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OROGRESSBAR_H__)
#define __OROGRESSBAR_H__

class OProgressBar : public OWindow
{
private:
	enum { self_type = WND_TYPE_PROGRESS };

public:
	OProgressBar();
	virtual ~OProgressBar();

public:
	// 创建编辑框
	virtual BOOL Create	
	(
		OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);

	virtual void Paint (LCD* pLCD);
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);

	// 设置整体范围
	BOOL SetRange (int nRange);

	// 设置当前进度
	BOOL SetPos (int nPos);
};

#endif // !defined(__OROGRESSBAR_H__)
