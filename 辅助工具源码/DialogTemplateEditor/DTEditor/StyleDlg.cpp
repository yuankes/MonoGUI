// StyleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DTEditor.h"
#include "StyleDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStyleDlg dialog


CStyleDlg::CStyleDlg( int nType, int nStyle, CWnd* pParent /*=NULL*/ )
	: CDialog(CStyleDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStyleDlg)
	m_bNoBorder = FALSE;
	m_bRoundEdge = FALSE;
	m_bPassword = FALSE;
	m_bGroup = FALSE;
	m_strType = _T("");
	m_bSolid = FALSE;
	m_bOriDefault = FALSE;
	m_bNoTitle = FALSE;
	m_bDisableIme = FALSE;
	m_bNoScroll = FALSE;
	m_bAutoDropdown = FALSE;
	m_bAutoOpenIme = FALSE;
	m_bIgnoreEnter = FALSE;
	m_bCloseButton = FALSE;
	//}}AFX_DATA_INIT

	m_nType = nType;
	m_nStyle = nStyle;

	// 根据类型确定m_strType应当显示的内容
	switch( m_nType )
	{
	case WND_TYPE_DIALOG:
		m_strType = "对话框";
		break;
	case WND_TYPE_STATIC:
		m_strType = "静态文本";
		break;
	case WND_TYPE_BUTTON:
		m_strType = "按钮";
		break;
	case WND_TYPE_EDIT:
		m_strType = "编辑框";
		break;
	case WND_TYPE_LIST:
		m_strType = "列表框";
		break;
	case WND_TYPE_COMBO:
		m_strType = "组合框";
		break;
	case WND_TYPE_PROGRESS:
		m_strType = "进度条";
		break;
	case WND_TYPE_IMAGE_BUTTON:
		m_strType = "图像按钮";
		break;
	case WND_TYPE_CHECK_BOX:
		m_strType = "复选框";
	default:{}
	}
}


void CStyleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStyleDlg)
	DDX_Control(pDX, IDC_CHECK13, m_checkCloseButton);
	DDX_Control(pDX, IDC_CHECK12, m_checkIgnoreEnter);
	DDX_Control(pDX, IDC_CHECK11, m_checkAutoOpenIme);
	DDX_Control(pDX, IDC_CHECK10, m_checkAutoDropdown);
	DDX_Control(pDX, IDC_CHECK09, m_checkNoScroll);
	DDX_Control(pDX, IDC_CHECK08, m_checkDiasbleIme);
	DDX_Control(pDX, IDC_CHECK07, m_checkNoTitle);
	DDX_Control(pDX, IDC_CHECK06, m_checkOriDefault);
	DDX_Control(pDX, IDC_CHECK05, m_checkSolid);
	DDX_Control(pDX, IDC_CHECK04, m_checkGroup);
	DDX_Control(pDX, IDC_CHECK03, m_checkPassword);
	DDX_Control(pDX, IDC_CHECK02, m_checkRoundEdge);
	DDX_Control(pDX, IDC_CHECK01, m_checkNoBounder);
	DDX_Check(pDX, IDC_CHECK01, m_bNoBorder);
	DDX_Check(pDX, IDC_CHECK02, m_bRoundEdge);
	DDX_Check(pDX, IDC_CHECK03, m_bPassword);
	DDX_Check(pDX, IDC_CHECK04, m_bGroup);
	DDX_Text(pDX, IDC_EDIT_TYPE, m_strType);
	DDX_Check(pDX, IDC_CHECK05, m_bSolid);
	DDX_Check(pDX, IDC_CHECK06, m_bOriDefault);
	DDX_Check(pDX, IDC_CHECK07, m_bNoTitle);
	DDX_Check(pDX, IDC_CHECK08, m_bDisableIme);
	DDX_Check(pDX, IDC_CHECK09, m_bNoScroll);
	DDX_Check(pDX, IDC_CHECK10, m_bAutoDropdown);
	DDX_Check(pDX, IDC_CHECK11, m_bAutoOpenIme);
	DDX_Check(pDX, IDC_CHECK12, m_bIgnoreEnter);
	DDX_Check(pDX, IDC_CHECK13, m_bCloseButton);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStyleDlg, CDialog)
	//{{AFX_MSG_MAP(CStyleDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStyleDlg message handlers

void CStyleDlg::OnOK() 
{
	UpdateData( TRUE );

	// 根据选项确定m_nStyle的值
	m_nStyle = 0x0;
	if( m_bNoBorder )
		m_nStyle |= 0x0001;
	if( m_bRoundEdge )
		m_nStyle |= 0x0002;
	if( m_bPassword )
		m_nStyle |= 0x0004;
	if( m_bGroup )
		m_nStyle |= 0x0008;
	if( m_bSolid )
		m_nStyle |= 0x0010;
	if( m_bOriDefault )
		m_nStyle |= 0x0020;
	if( m_bNoTitle )
		m_nStyle |= 0x0040;
	if( m_bDisableIme )
		m_nStyle |= 0x0080;
	if( m_bNoScroll )
		m_nStyle |= 0x0100;
	if( m_bAutoDropdown )
		m_nStyle |= 0x0200;
	if( m_bAutoOpenIme )
		m_nStyle |= 0x0400;
	if( m_bIgnoreEnter )
		m_nStyle |= 0x0800;
	if( m_bCloseButton )
		m_nStyle |= 0x1000;

	CDialog::OnOK();
}

void CStyleDlg::OnCancel() 
{
	CDialog::OnCancel();
}

BOOL CStyleDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// 根据控件风格确定各个check框的数值
	m_bNoBorder = FALSE;
	m_bRoundEdge = FALSE;
	m_bPassword = FALSE;
	m_bGroup = FALSE;
	m_bSolid = FALSE;
	m_bOriDefault = FALSE;
	m_bNoTitle = FALSE;
	m_bDisableIme = FALSE;
	m_bNoScroll = FALSE;
	m_bAutoDropdown = FALSE;
	m_bAutoOpenIme = FALSE;
	m_bIgnoreEnter = FALSE;

	if( ( m_nStyle & 0x0001 ) > 0 )
		m_bNoBorder = TRUE;
	if( ( m_nStyle & 0x0002 ) > 0 )
		m_bRoundEdge = TRUE;
	if( ( m_nStyle & 0x0004 ) > 0 )
		m_bPassword = TRUE;
	if( ( m_nStyle & 0x0008 ) > 0 )
		m_bGroup = TRUE;
	if( ( m_nStyle & 0x0010 ) > 0 )
		m_bSolid = TRUE;
	if( ( m_nStyle & 0x0020 ) > 0 )
		m_bOriDefault = TRUE;
	if( ( m_nStyle & 0x0040 ) > 0 )
		m_bNoTitle = TRUE;
	if( ( m_nStyle & 0x0080 ) > 0 )
		m_bDisableIme = TRUE;
	if( ( m_nStyle & 0x0100 ) > 0 )
		m_bNoScroll = TRUE;
	if( ( m_nStyle & 0x0200 ) > 0 )
		m_bAutoDropdown = TRUE;
	if( ( m_nStyle & 0x0400 ) > 0 )
		m_bAutoOpenIme = TRUE;
	if( ( m_nStyle & 0x0800 ) > 0 )
		m_bIgnoreEnter = TRUE;
	if( ( m_nStyle & 0x1000 ) > 0 )
		m_bCloseButton = TRUE;

	// 根据控件类型确定选项的使能
	switch( m_nType )
	{
	case WND_TYPE_DIALOG:
		{
			m_checkNoBounder.EnableWindow( FALSE );
			m_checkRoundEdge.EnableWindow( TRUE );
			m_checkPassword.EnableWindow( FALSE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( TRUE );
			m_checkOriDefault.EnableWindow( FALSE );
			m_checkNoTitle.EnableWindow( TRUE );
			m_checkDiasbleIme.EnableWindow( FALSE );
			m_checkNoScroll.EnableWindow( FALSE );
			m_checkAutoDropdown.EnableWindow( FALSE );
			m_checkAutoOpenIme.EnableWindow( FALSE );
			m_checkIgnoreEnter.EnableWindow( FALSE );
			m_checkCloseButton.EnableWindow( TRUE );
			m_bNoBorder = FALSE;
			m_bPassword = FALSE;
			m_bGroup = FALSE;
			m_bOriDefault = FALSE;
			m_bDisableIme = FALSE;
			m_bNoScroll = FALSE;
			m_bAutoDropdown = FALSE;
			m_bAutoOpenIme = FALSE;
			m_bIgnoreEnter = FALSE;
		}
		break;
	case WND_TYPE_STATIC:
		{
			m_checkNoBounder.EnableWindow( TRUE );
			m_checkRoundEdge.EnableWindow( TRUE );
			m_checkPassword.EnableWindow( FALSE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( FALSE );
			m_checkOriDefault.EnableWindow( FALSE );
			m_checkNoTitle.EnableWindow( FALSE );
			m_checkDiasbleIme.EnableWindow( FALSE );
			m_checkNoScroll.EnableWindow( FALSE );
			m_checkAutoDropdown.EnableWindow( FALSE );
			m_checkAutoOpenIme.EnableWindow( FALSE );
			m_checkIgnoreEnter.EnableWindow( FALSE );
			m_checkCloseButton.EnableWindow( FALSE );
			m_bPassword = FALSE;
			m_bGroup = FALSE;
			m_bSolid = FALSE;
			m_bOriDefault = FALSE;
			m_bNoTitle = FALSE;
			m_bDisableIme = FALSE;
			m_bNoScroll = FALSE;
			m_bAutoDropdown = FALSE;
			m_bAutoOpenIme = FALSE;
			m_bIgnoreEnter = FALSE;
			m_bCloseButton = FALSE;
		}
		break;
	case WND_TYPE_BUTTON:
		{
			m_checkNoBounder.EnableWindow( FALSE );
			m_checkRoundEdge.EnableWindow( TRUE );
			m_checkPassword.EnableWindow( FALSE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( FALSE );
			m_checkOriDefault.EnableWindow( TRUE );
			m_checkNoTitle.EnableWindow( FALSE );
			m_checkDiasbleIme.EnableWindow( FALSE );
			m_checkNoScroll.EnableWindow( FALSE );
			m_checkAutoDropdown.EnableWindow( FALSE );
			m_checkAutoOpenIme.EnableWindow( FALSE );
			m_checkIgnoreEnter.EnableWindow( FALSE );
			m_checkCloseButton.EnableWindow( FALSE );
			m_bNoBorder = FALSE;
			m_bPassword = FALSE;
			m_bGroup = FALSE;
			m_bSolid = FALSE;
			m_bNoTitle = FALSE;
			m_bDisableIme = FALSE;
			m_bNoScroll = FALSE;
			m_bAutoDropdown = FALSE;
			m_bAutoOpenIme = FALSE;
			m_bIgnoreEnter = FALSE;
			m_bCloseButton = FALSE;
		}
		break;
	case WND_TYPE_EDIT:
		{
			m_checkNoBounder.EnableWindow( TRUE );
			m_checkRoundEdge.EnableWindow( FALSE );
			m_checkPassword.EnableWindow( TRUE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( FALSE );
			m_checkOriDefault.EnableWindow( FALSE );
			m_checkNoTitle.EnableWindow( FALSE );
			m_checkDiasbleIme.EnableWindow( TRUE );
			m_checkNoScroll.EnableWindow( FALSE );
			m_checkAutoDropdown.EnableWindow( FALSE );
			m_checkAutoOpenIme.EnableWindow( TRUE );
			m_checkIgnoreEnter.EnableWindow( TRUE );
			m_checkCloseButton.EnableWindow( FALSE );
			m_bRoundEdge = FALSE;
			m_bGroup = FALSE;
			m_bSolid = FALSE;
			m_bOriDefault = FALSE;
			m_bNoTitle = FALSE;
			m_bNoScroll = FALSE;
			m_bAutoDropdown = FALSE;
			m_bCloseButton = FALSE;
		}
		break;
	case WND_TYPE_LIST:
		{
			m_checkNoBounder.EnableWindow( TRUE );
			m_checkRoundEdge.EnableWindow( FALSE );
			m_checkPassword.EnableWindow( FALSE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( TRUE );
			m_checkOriDefault.EnableWindow( FALSE );
			m_checkNoTitle.EnableWindow( FALSE );
			m_checkDiasbleIme.EnableWindow( FALSE );
			m_checkNoScroll.EnableWindow( TRUE );
			m_checkAutoDropdown.EnableWindow( FALSE );
			m_checkAutoOpenIme.EnableWindow( FALSE );
			m_checkIgnoreEnter.EnableWindow( TRUE );
			m_checkCloseButton.EnableWindow( FALSE );
			m_bRoundEdge = FALSE;
			m_bPassword = FALSE;
			m_bGroup = FALSE;
			m_bOriDefault = FALSE;
			m_bNoTitle = FALSE;
			m_bDisableIme = FALSE;
			m_bAutoDropdown = FALSE;
			m_bAutoOpenIme = FALSE;
			m_bCloseButton = FALSE;
		}
		break;
	case WND_TYPE_COMBO:
		{
			m_checkNoBounder.EnableWindow( FALSE );
			m_checkRoundEdge.EnableWindow( FALSE );
			m_checkPassword.EnableWindow( FALSE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( FALSE );
			m_checkOriDefault.EnableWindow( FALSE );
			m_checkNoTitle.EnableWindow( FALSE );
			m_checkDiasbleIme.EnableWindow( FALSE );
			m_checkNoScroll.EnableWindow( FALSE );
			m_checkAutoDropdown.EnableWindow( TRUE );
			m_checkAutoOpenIme.EnableWindow( FALSE );
			m_checkIgnoreEnter.EnableWindow( FALSE );
			m_checkCloseButton.EnableWindow( FALSE );
			m_bNoBorder = FALSE;
			m_bRoundEdge = FALSE;
			m_bPassword = FALSE;
			m_bGroup = FALSE;
			m_bSolid = FALSE;
			m_bOriDefault = FALSE;
			m_bNoTitle = FALSE;
			m_bDisableIme = FALSE;
			m_bNoScroll = FALSE;
			m_bAutoOpenIme = FALSE;
			m_bIgnoreEnter = FALSE;
			m_bCloseButton = FALSE;
		}
		break;
	case WND_TYPE_PROGRESS:
		{
			m_checkNoBounder.EnableWindow( TRUE );
			m_checkRoundEdge.EnableWindow( FALSE );
			m_checkPassword.EnableWindow( FALSE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( FALSE );
			m_checkOriDefault.EnableWindow( FALSE );
			m_checkNoTitle.EnableWindow( FALSE );
			m_checkDiasbleIme.EnableWindow( FALSE );
			m_checkNoScroll.EnableWindow( FALSE );
			m_checkAutoDropdown.EnableWindow( FALSE );
			m_checkAutoOpenIme.EnableWindow( FALSE );
			m_checkIgnoreEnter.EnableWindow( FALSE );
			m_checkCloseButton.EnableWindow( FALSE );
			m_bRoundEdge = FALSE;
			m_bPassword = FALSE;
			m_bGroup = FALSE;
			m_bSolid = FALSE;
			m_bOriDefault = FALSE;
			m_bNoTitle = FALSE;
			m_bDisableIme = FALSE;
			m_bNoScroll = FALSE;
			m_bAutoDropdown = FALSE;
			m_bAutoOpenIme = FALSE;
			m_bIgnoreEnter = FALSE;
			m_bCloseButton = FALSE;
		}
		break;
	case WND_TYPE_IMAGE_BUTTON:
		{
			m_checkNoBounder.EnableWindow( FALSE );
			m_checkRoundEdge.EnableWindow( FALSE );
			m_checkPassword.EnableWindow( FALSE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( FALSE );
			m_checkOriDefault.EnableWindow( FALSE );
			m_checkNoTitle.EnableWindow( FALSE );
			m_checkDiasbleIme.EnableWindow( FALSE );
			m_checkNoScroll.EnableWindow( FALSE );
			m_checkAutoDropdown.EnableWindow( FALSE );
			m_checkAutoOpenIme.EnableWindow( FALSE );
			m_checkIgnoreEnter.EnableWindow( FALSE );
			m_checkCloseButton.EnableWindow( FALSE );
			m_bNoBorder = FALSE;
			m_bRoundEdge = FALSE;
			m_bPassword = FALSE;
			m_bGroup = FALSE;
			m_bSolid = FALSE;
			m_bOriDefault = FALSE;
			m_bNoTitle = FALSE;
			m_bDisableIme = FALSE;
			m_bNoScroll = FALSE;
			m_bAutoDropdown = FALSE;
			m_bAutoOpenIme = FALSE;
			m_bIgnoreEnter = FALSE;
			m_bCloseButton = FALSE;
		}
		break;
	case WND_TYPE_CHECK_BOX:
		{
			m_checkNoBounder.EnableWindow( FALSE );
			m_checkRoundEdge.EnableWindow( FALSE );
			m_checkPassword.EnableWindow( FALSE );
			m_checkGroup.EnableWindow( FALSE );
			m_checkSolid.EnableWindow( FALSE );
			m_checkOriDefault.EnableWindow( FALSE );
			m_checkNoTitle.EnableWindow( FALSE );
			m_checkDiasbleIme.EnableWindow( FALSE );
			m_checkNoScroll.EnableWindow( FALSE );
			m_checkAutoDropdown.EnableWindow( FALSE );
			m_checkAutoOpenIme.EnableWindow( FALSE );
			m_checkIgnoreEnter.EnableWindow( FALSE );
			m_checkCloseButton.EnableWindow( FALSE );
			m_bNoBorder = FALSE;
			m_bRoundEdge = FALSE;
			m_bPassword = FALSE;
			m_bGroup = FALSE;
			m_bSolid = FALSE;
			m_bOriDefault = FALSE;
			m_bNoTitle = FALSE;
			m_bDisableIme = FALSE;
			m_bNoScroll = FALSE;
			m_bAutoDropdown = FALSE;
			m_bAutoOpenIme = FALSE;
			m_bIgnoreEnter = FALSE;
			m_bCloseButton = FALSE;
		}
		break;
	default:{}
	}
	//

	UpdateData( FALSE );
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
